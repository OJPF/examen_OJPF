/**
 * 
 */
package Examen;

/**
 * @author Oriol
 *
 */
public class programador extends empleado {

	
	// Definición de atributos
	
	private int lineasDeCodigoPorHora;
	private String lenguajeDominante;
	
	
	// Constructores
	
	// Constructor por defecto
	public programador(){
		this.lineasDeCodigoPorHora=250;
		this.lenguajeDominante="Python";
		}
	
	// Constructor con valores suyos + los de la clase principal
	
    public programador(String nombre, String cedula, int edad, boolean casado, double salario, int lineasDeCodigoPorHora, String lenguajeDominante ){
    	super (nombre,cedula,edad,casado,salario);
		this.lineasDeCodigoPorHora=lineasDeCodigoPorHora;
		this.lenguajeDominante=lenguajeDominante;
    }
	// Constructor con valores entrados por pantalla
		
		public programador(int lineasDeCodigoPorHora, String lenguajeDominante){
			this.lineasDeCodigoPorHora=lineasDeCodigoPorHora;
			this.lenguajeDominante=lenguajeDominante;
			}

		
		// Exportar los datos
		public int getLineasDeCodigoPorHora() {
			return lineasDeCodigoPorHora;
		}

		public void setLineasDeCodigoPorHora(int lineasDeCodigoPorHora) {
			this.lineasDeCodigoPorHora = lineasDeCodigoPorHora;
		}

		public String getLenguajeDominante() {
			return lenguajeDominante;
		}

		public void setLenguajeDominante(String lenguajeDominante) {
			this.lenguajeDominante = lenguajeDominante;
		}


}

