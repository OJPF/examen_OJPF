/**
 * 
 */
package Examen;

/**
 * @author Oriol
 *
 */
public class empleado {

	
	//Creamos las variables
	private String nombre;
	private String cedula;
	private int edad;
	private boolean casado;
	private double salario;
	
	// Creaci�n de los constructores
	
	// Constructor por defecto
	public empleado(){
		this.nombre= "Antonio Ponte";
		this.cedula="abcd19223";
		this.edad=18;
		this.casado=false;
		this.salario=1500;
	}
	// Constructor indicando parametros

	public empleado(String nombre, String cedula, int edad, boolean casado, double salario){
		this.nombre= "Antonio Ponte";
		this.cedula="abcd19223";
		this.edad=18;
		this.casado=false;
		this.salario=1500;
	}
	//Metodo para la edad:
	
	/* {
        if(edad) <=21{
            System.out.println("Principiante");
        }if (edad) >=22<=35{
        	System.out.println("Intermedio")}	
		if (edad) >35{
	System.out.println("Senior")}
}	
	}
	{
		System.out.println("Los datos del empleado son" +nombre +"," +cedula+"," +edad+"," +casado+"," +salario);
	}
	*/
	
	// Exportar datos
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public boolean isCasado() {
		return casado;
	}

	public void setCasado(boolean casado) {
		this.casado = casado;
	}

	public double getSalario() {
		return salario;
	}

	public void setSalario(double salario) {
		this.salario = salario;
	}
	
}
